const Alexa = require('ask-sdk');

const {
    LaunchRequestHandler,
    AmenitiesHandler,
    // RasaHandler,
    // CancelAndStopIntentHandler,
    FallbackHandler,
    // SessionEndedRequestHandler,
    // SystemExceptionHandler,
    // ErrorHandler
} = require('./intentHandlers/index');

const {
    ErrorHandler,
    SystemExceptionHandler,
    SessionEndedRequestHandler
} = require('./intentHandlers/commonHandlers');

const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
    .addRequestHandlers(
        LaunchRequestHandler,
        AmenitiesHandler,
        // RasaHandler,
        // CancelAndStopIntentHandler,
        FallbackHandler,
        SessionEndedRequestHandler,
        SystemExceptionHandler
    )
    .addErrorHandlers(ErrorHandler)
    .create();

// exports.handler = skillBuilder
//     .addRequestHandlers(
//         LaunchRequestHandler,
//         // SessionEndedRequestHandler,
//         // SystemExceptionHandler
//     )
//     .addErrorHandlers(ErrorHandler)
//     .create();

// alexa code
// const alexa = require('ask-sdk');
// const skillBuilder = alexa.SkillBuilders.custom();




// const LaunchRequestHandler = {
//     canHandle(handlerInput) {
//         return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
//     },
//     async handle(handlerInput) {
//         return handlerInput.responseBuilder
//             .speak("Hello! How may I help you?")
//             .reprompt('text')
//             .getResponse();
//     }
// };

// exports.handler = skillBuilder
//     .addRequestHandlers(
//         LaunchRequestHandler,
//         SessionEndedRequestHandler,
//         SystemExceptionHandler
//     )
//     .addErrorHandlers(ErrorHandler)
//     .create();