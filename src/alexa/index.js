const skillBuilder = require('./skillBuilder');
const router = require('express').Router();
const bodyParser = require('body-parser')

router.use(bodyParser.json());
router.get('/', (req, res) => res.json({ message: "Howdy from Alexa" }));
router.post('/', (req, res) => {
    console.log('Alexa Post Query')
    handlePostAlexaQuery({ req, res });
})

async function handlePostAlexaQuery({ req, res, customReq = null }) {
    try {
        if (customReq) {
            req = {};
            req.body = customReq;
        }
        // console.log("req.body", req.body);
        // console.log('req.body.request.type ALEXA HANDLER------', req.body.request);
        // console.log('req.body.request.type ALEXA HANDLER------', new Date());

        if (!(req.body.session.application.applicationId === 'amzn1.ask.skill.c91a68cc-e810-41c8-921b-73b56efa8f0a')) {
            return res.status(400);
        } else if (req.body.request.type === 'LaunchRequest') {
            // console.log("req.body===============", req.body);
            // req.body.request.type = 'xyz'
            // console.log("req.body===============", req.body);

            return skillBuilder.handler
                .invoke(req.body)
                .then(responseBody => res.json(responseBody))
                .catch((error) => {
                    console.error(error);
                    return res.status(500).send('Error during the request');
                });
        }
    } catch (err) {
        console.log("err connection")
    }
}

module.exports = router;