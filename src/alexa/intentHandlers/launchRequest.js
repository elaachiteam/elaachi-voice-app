const { genAndSendResponse } = require('../../responseHandlers/genAndSendResponse');

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    async handle(handlerInput) {
        let intentName = 'LaunchMessage';
        let attribute = {};
        attribute.anythingElse = true;
        let speechText = await genAndSendResponse(handlerInput, 'alexa', 'Completed', intentName, '', attribute);
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
}

module.exports = { LaunchRequestHandler };