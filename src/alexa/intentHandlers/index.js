const {AmenitiesHandler} = require('./amenitiesIntent');
const { LaunchRequestHandler } = require('./launchRequest');
const { FallbackHandler } = require('./FallbackHandler');

module.exports = {
    AmenitiesHandler,
    LaunchRequestHandler,
    FallbackHandler
};