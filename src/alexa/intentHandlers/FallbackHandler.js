const FallbackHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent'
    },
    async handle(handlerInput) {
        // const FallbackIntentResponse = await FallbackIntentCall(alexa, handlerInput);
        // return FallbackIntentResponse;
    },
};

module.exports = {
    FallbackHandler
}