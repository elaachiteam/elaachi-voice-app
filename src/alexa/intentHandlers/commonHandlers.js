const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        const { requestEnvelope } = handlerInput
        const request = requestEnvelope.request;
        console.log(`Error handled: ${error.message}`);
        console.log(`${request.reason}: ${request.error.type}, ${request.error.message}`)
        const message = 'Sorry, I missed what you said. Could you please repeat again? ';

        return handlerInput.responseBuilder
            .speak(message)
            .reprompt(message)
            .getResponse();
    },
};


const SystemExceptionHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'System.ExceptionEncountered';
    },
    handle(handlerInput) {
        console.log(`System exception encountered: ${handlerInput.requestEnvelope.request} ${handlerInput.requestEnvelope.request.reason}`);
    },
};

const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

        return handlerInput.responseBuilder.getResponse();
    },
};

module.exports = {
    ErrorHandler,
    SystemExceptionHandler,
    SessionEndedRequestHandler
}