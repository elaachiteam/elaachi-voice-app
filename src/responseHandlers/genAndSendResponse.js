const { randomArray } = require('../helperFunctions/randomArray');
const { getResponseFromElaachi } = require('../integrations/APICalls/getResponse');

async function genAndSendResponse(handlerInput, sourceDevice, dialogState, intentName, entities, attributes) {
    let speechText = ``;
    let responses = ``;
    let repromptText = ``;
    if (dialogState === 'Completed') {
        // data required is fulfilled
        responses = await getResponseFromElaachi(intentName);
        speechText = await randomArray(responses.data.speechText);
        console.log("SPEECHTEXT", speechText);
        return speechText;
    } else {
        // follow-up questions

    }

    // if(sourceDevice === 'alexa') {
    //     console.log("SPEECHTEXT ALEXA", speechText);

    //     return handlerInput.responseBuilder
    //     .speak(speechText)
    //     .reprompt(repromptText)
    //     .getResponse();
    // } else if (sourceDevice === 'google') {

    // } else if(sourceDevice === 'chatbot') {
    //     // if in future
    // } else {
    //     // please specify the source device
    // }
}

module.exports = {
    genAndSendResponse
}