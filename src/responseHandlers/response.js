module.exports = {
    ANYTHING_ELSE: [
        `Would you like to have anything else? `,
        `Anything else I can help you with?`,
        `Can I help you with anything else? `,
        `Anything else may I help you with? `,
    ],
    GOOD_MORNING: [
        `Good Morning! How can I help you? `,
        `Hello! Good Morning! how may I help you? `,
        `Hey! Good Morning! How may I help you? `
    ],
    GOOD_EVENING: [
        `Good Evening! How can I help you? `,
        `Hello! Good Evening! how may I help you? `,
        `Hey! Good Evening! How may I help you? `
    ],
    GOOD_NIGHT: [
        `Good Night!`
    ],
    GOODBYE_MESSAGE: [
        `Have a nice day. `,
        `Thank you for using our services. Have a nice day.`
    ]
}