const axios = require('axios');
let { baseURL, hotelId } = require('../config/creds');
async function getFAQResponse(setName, topicName) {
    try {
        const results = axios.get(`${baseURL}/faq/getAllSets?hotelId=${hotelId}`).then(function (response) {
            console.log(response);
            let arr = response.data;
            let id;
            for (let i = 0; i < response.data.length; i++) {
                console.log("the setName is ", response.data[i].SetName, setName)
                if (response.data[i].SetName === setName) {
                    id = response.data[i]._id;
                }
            }
            console.log("the ID is ", id)
            try {
                const results = axios.get(`${baseURL}/faq/getAllSetQuestion?FAQSetID=${id}&hotelId=${hotelId}`).then(function (response) {
                    console.log(response);
                    let TextResponse;
                    let AudioResponse;
                    for (let i = 0; i < response.data.length; i++) {
                        if (response.data[i].Topic === topicName) {
                            TextResponse = response.data[i].TextResponse[0];
                        }
                    }
                    console.log("TextResponse", TextResponse);
                    return TextResponse;
                }).catch(function (error) {
                    console.log(error);
                });
                return results;
            } catch (err) {
                console.log('Error Occured while connecting with the second api!', err);
            }
        }).catch(function (error) {
            console.log(error);
        });
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the first api!', err);
    }
}

module.exports = { getFAQResponse };