const { baseURL, hotelId } = require('../config/creds');
const axios = require('axios');

async function searchFoodItem(foodItem) {
    try {
        const result = axios.post(`${baseURL}/food/searchMenu`, {
            "item": foodItem,
            "hotelId": hotelId
        }).then(response => {
            console.log(response);
            return response;
        }).catch(err => {
            console.log(err);
        });
        return result;
    } catch (err) {
        console.log("trouble calling searchfood")
    }
}

module.exports = {
    searchFoodItem
}