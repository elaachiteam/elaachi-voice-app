const { baseURL, hotelId } = require('../../config/creds');
const axios = require('axios');
async function checkReqStatus(roomNumber, requestType) {
    try {
        const results = axios.get(`${baseURL}/orders/checkStatus?roomNumber=${roomNumber}&requestType=${requestType}&hotelId=${hotelId} `)
        .then(response => {
            console.log(response);
            return response.data;
        }).catch(err => {
            console.log(response);
        });
        return results;
    } catch (err) {
        console.log(err);
    }
}

module.exports = {checkReqStatus};