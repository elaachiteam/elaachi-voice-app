const axios = require('axios');
const { baseURL, hotelId } = require('../config/creds');
async function registerFeedback(Feedback) {
    let deviceId = 2;
    const roomDetails = await getRoomDetails(deviceId);
    let roomNum = roomDetails.data.response._id;
    const results = await axios.post(`${baseURL}/feedback/registerFeedback?hotelId=${hotelId}`, {
        roomNumber: roomNum,
        Feedback: Feedback
    }).then((response) => {
        return response;
    }).catch((err) => {
        console.log("error connecting with register feedback API ", err);
    });
    return results;
}

async function getRoomDetails(deviceId) {
    try {
        const results = axios.get(`${baseURL}/room/getRoomInfo?deviceId=2&hotelId=${hotelId}`).then(function (response) {
            console.log(response);
            return response;
        }).catch(function (error) {
            console.log(error);
        });
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the getRoomInfo api!')
    }
}

module.exports = { registerFeedback }