const { baseURL, hotelId } = require('../config/creds');
const axios = require('axios');
const { getResponse } = require('./getResponse');

async function registerRequest(requestTypeValue, requestDepartment, slotValue, deviceId, intentName, type) {
    try {
        const roomDetails = await getRoomDetails(deviceId);
        let roomNum = roomDetails.data.response._id;
        const requestId = new Date().valueOf();
        console.log('HERE', requestId);
        const results = await axios.post(`${baseURL}/orders/createRequest`, {
            // requestID: requestId,
            requestType: requestTypeValue,
            roomNumber: roomNum,
            requestDepartment: requestDepartment,
            slotValues: slotValue,
            requestStatus: "Pending",
            estimatedTime: "not assigned",
            requestTimeline: "Order Received",
            hotelId: hotelId,
            type: type
        }).then(async function (response) {
            const responseData = await getResponse(intentName);

            console.log(response, responseData);
            return responseData.data;
        }).catch(function (error) {
            console.log(error);
        });
        console.log('RESULT', results)
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the register request api!')
    }
}

async function getRoomDetails(deviceId) {
    try {
        const results = axios.get(`${baseURL}/room/getRoomInfo?deviceId=2&hotelId=${hotelId}`).then(function (response) {
            console.log(response);
            return response;
        }).catch(function (error) {
            console.log(error);
        });
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the getRoomInfo api!')
    }
}

module.exports = {
    registerRequest
}