const axios = require('axios');
let { baseURL, hotelId } = require('../config/creds');
async function getFeedbackQuestions() {
    try {
        const results = axios.get(`${baseURL}/feedback/getQuestions?hotelId=${hotelId}`).then(function (response) {
            console.log(response);
            return response.data;
        }).catch(function (error) {
            console.log(error);
        });
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the getResponse api!', err);
    }
}

module.exports = {getFeedbackQuestions};