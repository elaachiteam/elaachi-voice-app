const axios = require('axios');
const { baseURL , hotelId }  = require('../../config/creds');


async function getCategories() {
    try {
        const result = axios.get(`${baseURL}/spa?hotelId=${hotelId}`).then(response => {
            console.log(response);
            return response;
        }).catch(err => {
            console.log(err);
        });
        return result;
    } catch (err) {
        console.log("trouble calling searchfood")
    }
}

module.exports = {
    getCategories
}