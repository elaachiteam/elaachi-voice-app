const axios = require('axios');
const { baseURL , hotelId }  = require('../../config/creds');

async function getService() {
    try {
        const result = axios.get(`${baseURL}/food/searchMenu`).then(response => {
            console.log(response);
            return response;
        }).catch(err => {
            console.log(err);
        });
        return result;
    } catch (err) {
        console.log("trouble calling API", err)
    }
}

module.exports = {
    searchFoodItem
}