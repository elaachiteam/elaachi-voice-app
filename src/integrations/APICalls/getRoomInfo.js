const { baseURL, hotelId } = require('../config/creds');
const axios = require('axios');

async function getRoomInfo(deviceId) {
    try {
        const results = axios.get(`${baseURL}/room/getRoomInfo?deviceId=2&hotelId=${hotelId}`).then(function (response) {
            console.log(response);
            return response;
        }).catch(function (error) {
            console.log(error);
        });
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the getResponse api!', err);
    }
}

module.exports = {
    getRoomInfo
}