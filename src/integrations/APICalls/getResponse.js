const axios = require('axios');
let baseURL = 'https://dev-elaachi-server1.herokuapp.com';
let hotelId = `5e55f0e0b3cd335b58134042`;
async function getResponseFromElaachi(IntentName) {
    try {
        const results = axios.get(`${baseURL}/response/getResponseData?intentName=${IntentName}&hotelId=${hotelId}`).then(function (response) {
            console.log(response);
            return response;
        }).catch(function (error) {
            console.log(error);
        });
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the getResponse api!', err);
    }
}

module.exports = { getResponseFromElaachi };
