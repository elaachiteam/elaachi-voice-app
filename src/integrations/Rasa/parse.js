const auth = require('../../integrations/rasa/auth');
const parseAPI = require('../../integrations/rasa/parseAPI');

async function parseAlexaText(text, next) {
    let model = {};
    model.text = text;
    const authObject = await auth.authTokens(); // TODO: if parseAPI fails
    const parseObject = await parseAPI.parseText(model.text, authObject);
    return parseObject;
}

module.exports = {
    parseAlexaText
}