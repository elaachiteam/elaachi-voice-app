const express = require('express');
const bodyParser = require('body-parser')
const skillBuilder = require('./src/alexa/skillBuilder');

const alexaRoute = express.Router()
const verifier = require('alexa-verifier-middleware')


alexaRoute.use(bodyParser.json());
alexaRoute.get('/', (req, res) => res.json({ message: 'hello from alexa' }));
alexaRoute.post('/', async(req, res) => {
    console.log('fghjk-----====???')
    handlePostAlexaQuery({ req, res });
});



async function handlePostAlexaQuery({ req, res, customReq = null }) {
    try {
        console.log('fghjk-----====???', req)

        if (customReq) {
            req = {};
            req.body = customReq;
        }
        console.log("req.body", req.body);
        console.log('req.body.request.type ALEXA HANDLER------', req.body.request);
        console.log('req.body.request.type ALEXA HANDLER------', new Date());
        if (!(req.body.session.application.applicationId === 'amzn1.ask.skill.c91a68cc-e810-41c8-921b-73b56efa8f0a')) {
            return res.status(400);
        } else if (req.body.request.type === 'LaunchRequest') {
            console.log("req.body===============", req.body);

            return skillBuilder.handler
                .invoke(req.body)
                .then(responseBody => res.json(responseBody))
                .catch((error) => {
                    console.error(error);
                    return res.status(500).send('Error during the request');
                });
        }

    } catch (error) {
        console.log('*************', error)
    }
}



module.exports = alexaRoute;