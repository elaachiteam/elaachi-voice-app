// alexa code
const alexa = require('ask-sdk');
const skillBuilder = alexa.SkillBuilders.custom();



const SystemExceptionHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'System.ExceptionEncountered';
    },
    handle(handlerInput) {
        console.log(`System exception encountered: ${handlerInput.requestEnvelope.request} ${handlerInput.requestEnvelope.request.reason}`);
    },
};

const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

        return handlerInput.responseBuilder.getResponse();
    },
};

const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`Error handled: ${error.message}`);
        const message = 'Sorry, I missed what you said. Could you please repeat again? ';

        return handlerInput.responseBuilder
            .speak(message)
            .reprompt(message)
            .getResponse();
    },
};

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    async handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak("Hello! How may I help you?")
            .reprompt('text')
            .getResponse();
    }
};

exports.handler = skillBuilder
    .addRequestHandlers(
        LaunchRequestHandler,
        SessionEndedRequestHandler,
        SystemExceptionHandler
    )
    .addErrorHandlers(ErrorHandler)
    .create();