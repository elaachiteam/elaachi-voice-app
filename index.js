const express = require('express');
const app = express();
const alexaSkill = require('./src/alexa/index');
const assistant = require('./google')

app.use('/alexa', alexaSkill)
app.post('/google', assistant)

app.listen(process.env.PORT || 3001, () => {
    console.log('server started')
})